# Vous devez copier le texte ci-dessous et le coller dans le Grub de MultiSystem
# Attention vous devez utiliser la version de Clonezilla 64 bits : clonezilla-live-2.6.7-28-amd64
#
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-semiautomatique-64|292Mio|
menuentry "Clé de réemploi Emmabuntüs 64 bits (Installation semi-automatique)" {
search --set -f "/clonezilla-live-2.6.7-28-amd64.iso"
loopback loop "/clonezilla-live-2.6.7-28-amd64.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-amd64.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=fr_FR.UTF-8 keyboard-layouts=fr ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\"  ocs_live_run="/home/partimag/clone.sh" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-semiautomatique-64|292Mio|
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-automatique-64|292Mio|
menuentry "Clé de réemploi Emmabuntüs 64 bits (Installation automatique)" {
search --set -f "/clonezilla-live-2.6.7-28-amd64.iso"
loopback loop "/clonezilla-live-2.6.7-28-amd64.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-amd64.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=fr_FR.UTF-8 keyboard-layouts=fr ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\"  ocs_live_run="/home/partimag/clone.sh -a" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-automatique-64|292Mio|
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-sauvegarde-64|292Mio|
menuentry "Clé de réemploi Emmabuntüs 64 bits (Sauvegarde automatique du clone)" {
search --set -f "/clonezilla-live-2.6.7-28-amd64.iso"
loopback loop "/clonezilla-live-2.6.7-28-amd64.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-amd64.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=fr_FR.UTF-8 keyboard-layouts=fr ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\"  ocs_live_run="/home/partimag/save_clone.sh" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-amd64.iso|multisystem-clonezilla-emmabuntus-sauvegarde-64|292Mio|
