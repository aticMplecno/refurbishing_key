# You need to copy the text below and and paste it into the MultiSystem Grub
# Please note you must use the 32-bit version of Clonezilla: clonezilla-live-2.6.7-28-686
#
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-semiautomatic-32|277Mio|
menuentry "Emmabuntüs refurbishing key 32 bits (Semi-automatic install)" {
search --set -f "/clonezilla-live-2.6.7-28-i686.iso"
loopback loop "/clonezilla-live-2.6.7-28-i686.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-i686.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=en_US.UTF-8 keyboard-layouts=en ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\" ocs_live_run="/home/partimag/clone.sh" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-semiautomatic-32|277Mio|
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-automatic-32|277Mio|
menuentry "Emmabuntüs refurbishing key 32 bits (Automatic install)" {
search --set -f "/clonezilla-live-2.6.7-28-i686.iso"
loopback loop "/clonezilla-live-2.6.7-28-i686.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-i686.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=en_US.UTF-8 keyboard-layouts=en ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\" ocs_live_run="/home/partimag/clone.sh -a" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-automatic-32|277Mio|
#MULTISYSTEM_MENU_DEBUT|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-save-32|277Mio|
menuentry "Emmabuntüs refurbishing key 32 bits (Save clone)" {
search --set -f "/clonezilla-live-2.6.7-28-i686.iso"
loopback loop "/clonezilla-live-2.6.7-28-i686.iso"
linux (loop)/live/vmlinuz findiso=/clonezilla-live-2.6.7-28-i686.iso toram=filesystem.squashfs boot=live union=overlay username=user config components noswap edd=on nomodeset noprompt quiet splash locales=en_US.UTF-8 keyboard-layouts=en ocs_prerun=\"mount \$(blkid \| grep IMAGES \| cut -d : -f1) /home/partimag/\" ocs_live_run="/home/partimag/save_clone.sh" ocs_live_extra_param= ocs_live_batch=no gfxpayload=1024x768x16,1024x768 ip=frommedia i915.blacklist=yes radeonhd.blacklist=yes nouveau.blacklist=yes vmwgfx.blacklist=yes
initrd (loop)/live/initrd.img
}
#MULTISYSTEM_MENU_FIN|23-08-2020-12:25:24-893097238|clonezilla-live-2.6.7-28-i686.iso|multisystem-clonezilla-emmabuntus-save-32|277Mio|
