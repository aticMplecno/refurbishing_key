#!/bin/sh

# clone.sh --
#
#   This file allows the automatic reconditionning of a computer
#   by automating the installation with Clonezilla
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was tested and validated on Clonezilla live 3.0.1-8 i686/amd64
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

#DEBUG="true"
#BEEP_OFF="true"

# Volume sonore de Clonezilla en % au niveau de l'utilitaire amixer aka alsamixer
VOLUME=80

SEARCH_FILE=clonezilla-img
KEY_DRIVE=$(sudo blkid | grep LABEL=\"IMAGES\" | cut  -d ':' -f1)
UEFI_ON=0
SECUREBOOT_ON=0
LANGUE=$(echo ${LANG} | cut -d _ -f1)
CLONEZILLA_BATCH="-batch"
AUTOMATIC="false"
CLONE_AUTOMATIC_EXIST="false"
NUMBER_PART_EXTEND=0
CONFIG_FILE=clone.ini
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Lecture des options de la ligne de commande
# Read command line options
LINE_OPTION=$1

if [ -z ${DEBUG} ] ; then
    # Efface l'affichage précédant
    clear
fi

if [ ${LINE_OPTION} = "-a" ] ; then
    AUTOMATIC="true"
fi

if [ "$(which mokutil)" != "" ] ; then
    if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
        SECUREBOOT_ON=1
    fi
fi


if [ ${LANGUE} = fr ]
then

    message_info_emmabuntus="Clé USB de réemploi par Emmabuntüs (https://emmabuntus.org)"
    message_info_sources="Les sources sont disponibles sur http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Attention : La clé USB avec l'étiquette \"IMAGES\" n'a pas été indentifiée. Taper sur une touche quelconque pour sortir du script"
    message_trouve_usb_avant="Clé USB trouvée sur"
    message_trouve_usb_apres=", mais ce n'est pas son emplacement habituel, ce qui signifie que vous avez plusieurs disques durs sur ce système."
    message_selection_usb="Veuillez entrer le numéro correspondant au disque cible de l'installation :"
    message_mauvais_nombre_disque_dur="Attention : Vous n'avez pas entré un nombre pour selectionner le disque. Taper sur une touche quelconque pour sortir du script"
    message_mauvais_selection_disque_dur="Attention : Vous n'avez pas entré un nombre correspondant à un disque. Taper sur une touche quelconque pour sortir du script"
    message_usb_disque_dur_avant="Clé USB trouvée sur"
    message_usb_disque_dur_apres="et disque sélectionné sur "
    message_recherche_clone_uefi="Patientez un moment, nous recherchons les clones de type UEFI présents dans la clé de réemploi sur"
    message_recherche_clone_uefi_secureboot="Patientez un moment, nous recherchons les clones de type UEFI avec \"Secure Boot\" présents dans la clé de réemploi sur"
    message_recherche_clone_legacy="Patientez un moment, nous recherchons les clones de type \"Legacy\" présents dans la clé de réemploi sur"
    message_clone_abscent="Attention : Aucun clone n'a été trouvé sur la clé USB. Taper sur une touche quelconque pour sortir du script"
    message_clone_trouve="Ce clone a été trouvé sur la clé USB :"
    message_plusieurs_clones="Plusieurs clones ont été trouvés sur la clé USB, veuillez indiquer le numéro du clone à installer :"
    message_mauvais_nombre_clone="Attention : Vous n'avez pas entré un nombre correspondant à un clone. Taper sur une touche quelconque pour sortir du script"
    message_nom_clone="Nom du clone : "
    message_mauvais_nombre="Attention : Vous n'avez pas entré un nombre correspondant à un clone. Taper sur une touche quelconque pour sortir du script"
    message_clone_non_present="Attention : ce clone n'esite pas. Taper sur une touche quelconque pour sortir du script"
    message_info_partition_avant="/!\ Attention : voulez vous partitionner le disque /dev/"
    message_info_partition_apres=", toutes les données seront détruites ? [o/N] : "
    message_partition_ko="Abandon du partionnement !!!"
    message_mode_automatique="La clé de réemploi est en mode automatique"
    message_nom_clone_automatique="Nom du clone automatique sélectionné : "
    message_fichier_config_automatique_non_trouve="Fichier de configuration du clonage en mode automatique (${CONFIG_FILE}) non trouvé !!!"
    message_clone_automatique_non_trouve="Clone défini dans le fichier de configuration du clonage en mode automatique (${CONFIG_FILE}) non trouvé : "
    message_clone_automatique_vide="Aucun clone n'est défini dans le fichier de configuration du clonage en mode automatique (${CONFIG_FILE}) !!!"
    message_clone_automatique_non_disponible_uefi_32="Le clonage en automatique n'est pas disponible avec Clonezilla 32 bits en mode UEFI !!!"
    message_nombre="Saisissez un nombre ci-dessus, puis validez : "
    CODE_OK_MINUSCULE="o"
    CODE_OK_MAJUSCULE="O"

else

    message_info_emmabuntus="Emmabuntüs reusable USB key (https://emmabuntus.org)"
    message_info_sources="Sources are avbailable on http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Warning : The USB key with the \"IMAGES\" label was not found. Press any key to exit the script"
    message_trouve_usb_avant="USB Key found on"
    message_trouve_usb_apres=", but this is not it's usual location, which means that there are several hard drives on your computer."
    message_selection_usb="Please enter the number corresponding to the target disk of this install."
    message_mauvais_nombre_disque_dur="Warning : You did not enter a number to select the disk. Press any key to exit the script"
    message_mauvais_selection_disque_dur="Warning : You did not enter a number corresponding to a disk. Press any key to exit the script"
    message_usb_disque_dur_avant="USB Key found on"
    message_usb_disque_dur_apres="and selected hard drive on "
    message_recherche_clone_uefi="Please wait, we are looking for UEFI clones present on your re-se USB key sitting on"
    message_recherche_clone_uefi_secureboot="Please wait, we are looking for UEFI with \"Secure Boot\" clones present on your re-se USB key sitting on"
    message_recherche_clone_legacy="Please wait, we are looking for Legacy clones present on your re-use USB key sitting on "
    message_clone_abscent="Warning : No clone found on the USB key. Press any key to exit the script"
    message_clone_trouve="This clone was found on the USB key:"
    message_plusieurs_clones="Several clones were found on the USB key, please type the number of the clone you want to install :"
    message_mauvais_nombre_clone="Warning : You did not enter a number to select a clone. Press any key to exit the script"
    message_nom_clone="Clone name : "
    message_mauvais_nombre="Warning : You did not enter a number corresponding to a clone. Press any key to exit the script"
    message_clone_non_present="Warning : This clone does not exist. Press any key to exit the script"
    message_info_partition_avant="/!\ Warning : Do you want to partition the disk /dev/"
    message_info_partition_apres=", all data will be erased ? [y/N]: "
    message_partition_ko="Partitionning aborted !!!"
    message_mode_automatique="The re-use key is in automatic mode "
    message_nom_clone_automatique="Name of the selected automatic clone: "
    message_fichier_config_automatique_non_trouve="Automatic mode cloning configuration file (${CONFIG_FILE}) not found  !!!"
    message_clone_automatique_non_trouve="Clone defined in the automatic mode cloning configuration file (${CONFIG_FILE}) not found: "
    message_clone_automatique_vide="No clone is defined in the automatic mode cloning configuration file (${CONFIG_FILE}) !!!"
    message_clone_automatique_non_disponible_uefi_32="Automatic cloning is not available with 32-bit Clonezilla in UEFI mode !!!"
    message_nombre="Enter a number above, then validate: "
    CODE_OK_MINUSCULE="y"
    CODE_OK_MAJUSCULE="Y"

fi


echo ""
echo ${message_info_emmabuntus}
echo ${message_info_sources}
echo ""

if  [ ${AUTOMATIC} = "true" ] ; then
echo "${YELLOW}${message_mode_automatique}${NC}"
echo ""
fi

# Verrouillage du pavé numérique
setleds -D +num

# Active un son à la fin des opérations de Clonezilla
if [ -z ${BEEP_OFF} ] ; then

    # Activation du son de Clonzilla
    CLONEZILLA_BEEP="-ps"

    # Modification du volume de la carte son
    amixer -q -c 0 set Master ${VOLUME}%
    amixer -q -c 0 set PCM ${VOLUME}%

else
    CLONEZILLA_BEEP=""
fi



# Identification de la clé USB
# Finding out the USB Key
if [ -z ${KEY_DRIVE} ] ; then

   echo -n "${RED}${message_identification_usb_ko}${NC}"
   echo "\a"

   read read_no_correct_label_key

   exit 0

elif [ $(echo ${KEY_DRIVE} | grep sdb) ] ; then

    KEY_DRIVE_FOUND="/dev/sdb"

elif [ $(echo ${KEY_DRIVE} | grep sda) ] ; then

    KEY_DRIVE_FOUND="/dev/sda"

fi

# Identification du disque dur
# Finding the hard drive

find_disk=$(sudo fdisk -l | grep 'Disk /dev/sd\|Disk /dev/hd\|Disk /dev/mmcblk\|Disk /dev/hd\|Disk /dev/nvme' | cut -d : -f1 | cut -d " " -f2)
nb_find_disk=$(echo ${find_disk} | wc -w)

if [ ! -z ${DEBUG} ] ; then
    echo "KEY_DRIVE_FOUND=${KEY_DRIVE_FOUND}"
    echo "find_disk=${find_disk}"
    echo "nb_find_disk=${nb_find_disk}"
fi

if [ ${nb_find_disk} -eq 2 ] && [ -n ${KEY_DRIVE_FOUND}  ] ; then # Si 2 disques trouvés et que la clé USB a été identifiée

    for disk_name in ${find_disk}
    do
        if [ "${disk_name}" != ${KEY_DRIVE_FOUND} ] ; then
            HARD_DRIVE=$(echo ${disk_name} | cut -d / -f3)
        fi

    done

elif [ -n "${KEY_DRIVE}" ] ; then

    echo "${message_trouve_usb_avant} ${GREEN}${KEY_DRIVE}${NC}${message_trouve_usb_apres}"
    echo ${message_selection_usb}
    echo ""

    i=0
    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            disk_model=$(sudo fdisk -l ${disk_name} | grep "Disk model" | cut -d : -f2)
            disk_size=$(sudo fdisk -l ${disk_name} | grep "Disk " | grep sector | cut -d : -f2 | cut -d B -f1)
            i=$(($i+1))
            echo "${YELLOW}${i}${NC} - ${GREEN}${disk_name} - model: ${disk_model} - size: ${disk_size}B${NC}"
        fi

    done

    echo -n "${YELLOW}${message_nombre}${NC}"

    echo -n "${ORANGE}"
    read read_disk_number
    echo -n "${NC}"

    # Test si c'est un nombre
    # Check if the entry is a number
    if echo ${read_disk_number} | grep -qE '^[0-9]+$'; then

        echo ""

    else

        echo -n "${RED}${message_mauvais_nombre_disque_dur}${NC}"
        echo "\a"

        read read_no_correct_number

        exit 0
    fi

    i=0
    macth_number=0

    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            i=$(($i+1))
            if [ ${read_disk_number} -eq $i ] ; then
                HARD_DRIVE=${disk_name}
                macth_number=1
            fi
        fi
    done

    if [  ${macth_number} -eq 1 ] ; then

        HARD_DRIVE=$(echo ${HARD_DRIVE} | cut -d / -f3)

    else

        echo -n "${RED}${message_mauvais_selection_disque_dur}${NC}"
        echo "\a"

        read read_no_correct_number

        exit 0

    fi

fi

echo "${message_usb_disque_dur_avant} ${GREEN}${KEY_DRIVE}${NC} ${message_usb_disque_dur_apres}${GREEN}/dev/${HARD_DRIVE}${NC}"
echo ""

if test -d /sys/firmware/efi/ ; then

    # Mode UEFI
    UEFI_ON=1

    if [ ${SECUREBOOT_ON} -eq 0 ] ; then
    # Sans boot secure
        echo "${message_recherche_clone_uefi} ${GREEN}${KEY_DRIVE}${NC} ..."
    else
        # Avec boot secure
        echo "${message_recherche_clone_uefi_secureboot} ${GREEN}${KEY_DRIVE}${NC} ..."
    fi
else
    echo "${message_recherche_clone_legacy} ${GREEN}${KEY_DRIVE}${NC} ..."
fi

echo ""


# Positionnement sur la clé USB au niveau de clones
# Go to clones top dir
cd /home/partimag


# Recherche des dossiers contenant des clones
# Looking for clone folders
if test -d /sys/firmware/efi/ ; then
    # Mode UEFI
    if [ ${SECUREBOOT_ON} -eq 0 ] ; then
        # Sans boot secure
        find_clone_directory=$(find . -type f -name ${SEARCH_FILE} | grep -i UEFI | cut  -d '/' -f2)
    else
        # Avec boot secure
        find_clone_directory=$(find . -type f -name ${SEARCH_FILE} | grep -i UEFI_SB | cut  -d '/' -f2)
    fi
else
    find_clone_directory=$(find . -type f -name ${SEARCH_FILE} | grep -vi UEFI | cut  -d '/' -f2)
fi

nb_clone_directory=$(echo ${find_clone_directory} | wc -w)

# Détermination version Clonezilla
if [ $(uname -m) = "x86_64" ] ; then
    clonezilla_64bits="true"
else
    clonezilla_64bits="false"
fi

# Mode automatique
if  [ ${AUTOMATIC} = "true" ] ; then

    # Test si il y a un fichier de configuration des clones en mode automatique
    if test -f ${CONFIG_FILE} ; then

        if [ ${clonezilla_64bits} = "true" ] ; then

            if test -d /sys/firmware/efi/ ; then
                # Mode UEFI
                if [ ${SECUREBOOT_ON} -eq 0 ] ; then
                    # Sans boot secure
                    name_automatic_clone=$(cat ${CONFIG_FILE} | grep "CLONE_UEFI_64=" | cut  -d '=' -f2)
                    NUMBER_PART_EXTEND=$(cat ${CONFIG_FILE} | grep "CLONE_UEFI_64_NUMBER_PART_EXTEND=" | cut  -d '=' -f2)
                else
                    # Avec boot secure
                    name_automatic_clone=$(cat ${CONFIG_FILE} | grep "CLONE_UEFI_SB_64=" | cut  -d '=' -f2)
                    NUMBER_PART_EXTEND=$(cat ${CONFIG_FILE} | grep "CLONE_UEFI_SB_64_NUMBER_PART_EXTEND=" | cut  -d '=' -f2)
                fi
            else
                name_automatic_clone=$(cat ${CONFIG_FILE} | grep "CLONE_LEGACY_64=" | cut  -d '=' -f2)
                NUMBER_PART_EXTEND=$(cat ${CONFIG_FILE} | grep "CLONE_LEGACY_64_NUMBER_PART_EXTEND=" | cut  -d '=' -f2)
            fi

        else
            name_automatic_clone=$(cat ${CONFIG_FILE} | grep "CLONE_LEGACY_32=" | cut  -d '=' -f2)
            NUMBER_PART_EXTEND=$(cat ${CONFIG_FILE} | grep "CLONE_LEGACY_32_NUMBER_PART_EXTEND=" | cut  -d '=' -f2)
        fi

    else

        echo "${ORANGE}${message_fichier_config_automatique_non_trouve}${NC}"
        echo "\a"

    fi

    # Vérifie si un nom de clone est valide et si ce clone est présent dans la liste des clones trouvés sur la clé USB
    if test -n "${name_automatic_clone}" ; then

        for clone_name in ${find_clone_directory}
        do
            if [ ${name_automatic_clone} = ${clone_name} ] ; then
                CLONE_AUTOMATIC_EXIST="true"
            fi
        done

        if [ ${CLONE_AUTOMATIC_EXIST} = "false" ] ; then

            if [ ${clonezilla_64bits} = "false" ] && test -d /sys/firmware/efi/ ; then

                echo "${ORANGE}${message_clone_automatique_non_disponible_uefi_32}${NC}"
                echo "\a"

            else

                echo "${ORANGE}${message_clone_automatique_non_trouve} ${name_automatic_clone} !!!${NC}"
                echo "\a"

            fi

        fi

    else

        if [ ${clonezilla_64bits} = "false" ] && test -d /sys/firmware/efi/ ; then

            echo "${ORANGE}${message_clone_automatique_non_disponible_uefi_32}${NC}"
            echo "\a"

        else

            echo "${ORANGE}${message_clone_automatique_vide}${NC}"
            echo "\a"

        fi

    fi

fi

# Selection du clone quand on n'est pas en mode automatique
if [ ${CLONE_AUTOMATIC_EXIST} = "false" ] ; then

    if [ ${nb_clone_directory} -eq 0 ] ; then

        echo -n "${RED}${message_clone_abscent}${NC}"
        echo "\a"

        read read_no_clone

        exit 0

    elif [ ${nb_clone_directory} -eq 1 ] ; then

        echo -n "${message_clone_trouve} ${GREEN}${find_clone_directory}${NC}"
        echo ""

        NAME_CLONE=${find_clone_directory}

    else

        echo ${message_plusieurs_clones}
        echo ""

        i=0
        for clone_name in ${find_clone_directory}
        do
            i=$(($i+1))
            echo "${YELLOW}${i}${NC} - ${GREEN}${clone_name}${NC}"
        done

        if [ -z ${BEEP_OFF} ] ; then
            echo -n "\a"
            bip_send=1
        fi

        echo -n "${YELLOW}${message_nombre}${NC}"

        echo -n "${ORANGE}"
        read read_clone_number
        echo -n "${NC}"

        # echo "Clone number : ${read_clone_number}"


        # Test si c'est un nombre
        # Check if entry is a number
        if echo ${read_clone_number} | grep -qE '^[0-9]+$'; then

            echo ""

        else

            echo -n "${RED}${message_mauvais_nombre_clone}${NC}"
            echo "\a"

            read read_no_correct_number

            exit 0
        fi

        i=0
        macth_number=0

        for clone_name in ${find_clone_directory}
        do
            i=$(($i+1))
            if [ ${read_clone_number} -eq $i ] ; then
                NAME_CLONE=${clone_name}
                macth_number=1
            fi
        done

        if [  ${macth_number} -eq 1 ] ; then

            echo "${YELLOW}${message_nom_clone}${NC} ${GREEN}${NAME_CLONE}${NC}"
            echo ""

        else

            echo -n "${RED}${message_mauvais_nombre}${NC}"
            echo "\a"

            read read_no_correct_number

            exit 0

        fi

    fi

# Affichage du clone sélectionné en mode automatique
else

    NAME_CLONE=${name_automatic_clone}
    echo "${YELLOW}${message_nom_clone_automatique}${NC} ${GREEN}${NAME_CLONE}${NC}"
    echo ""

fi


# Test si le répertoire du clone existe sinon fin du script
# Chek if clone directory exists, else end of script

if ! test -d ./${NAME_CLONE} ; then

    echo -n "${RED}${message_clone_non_present}${NC}"
    echo "\a"

    read read_no_clone_directory

    exit 0

fi


# Partie active du script
# The active part of the script

# Demande de confirmation du clonage en mode non automatique
if [ ${CLONE_AUTOMATIC_EXIST} = "false" ] ; then

    if [ -z ${BEEP_OFF} ] && [ -z ${bip_send} ]  ; then
        echo -n "\a"
    fi

    echo ""
    echo -n "${YELLOW}${message_info_partition_avant}${HARD_DRIVE}${message_info_partition_apres}${NC}"

    echo -n "${ORANGE}"
    read read_1
    echo -n "${NC}"

    if [ "$read_1" = ${CODE_OK_MINUSCULE} ] || [ "$read_1" = ${CODE_OK_MAJUSCULE} ] ; then

         echo -n "${YELLOW}${message_info_partition_avant}${HARD_DRIVE}${message_info_partition_apres}${NC}"

         echo -n "${ORANGE}"
         read read_2
         echo -n "${NC}"

         if [ "$read_2" = ${CODE_OK_MINUSCULE} ] || [ "$read_2" =  ${CODE_OK_MAJUSCULE} ] ;  then

            /home/partimag/parted.sh ${HARD_DRIVE} ${UEFI_ON} ${NAME_CLONE} ${NUMBER_PART_EXTEND}

            if [ "$?" -eq 0 ]; then

                sudo /usr/sbin/ocs-sr -g auto -e1 auto -e2 ${CLONEZILLA_BATCH} -r -j2 -k -scr ${CLONEZILLA_BEEP} -p reboot restoredisk ${NAME_CLONE} ${HARD_DRIVE}

            fi

         else

            echo "${RED}${message_partition_ko}${NC}"
            echo -n "\a"

         fi

    else

         echo "${RED}${message_partition_ko}${NC}"
         echo -n "\a"

    fi

# Lancement du clonage en mode automatique
else

    /home/partimag/parted.sh ${HARD_DRIVE} ${UEFI_ON} ${NAME_CLONE} ${NUMBER_PART_EXTEND}


    if [ "$?" -eq 0 ]; then

        sudo /usr/sbin/ocs-sr -g auto -e1 auto -e2 ${CLONEZILLA_BATCH} -r -j2 -k -scr ${CLONEZILLA_BEEP} -p reboot restoredisk ${NAME_CLONE} ${HARD_DRIVE}

    fi

fi


